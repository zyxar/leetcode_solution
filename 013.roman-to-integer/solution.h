class Solution {
public:
    int romanToInt(string s) {
        size_t len = s.length();
        if (len == 0) return 0;
        if (len == 1) return num(s[0]);
        int sum = 0;
        for (int i = 0; i < len-1; ++i) {
            if (num(s[i]) < num(s[i+1])) sum -= num(s[i]);
            else sum += num(s[i]);
        }
        sum += num(s[len-1]);
        return sum;
    }
private:
    int num(char c) {
        switch (c) {
        case 'I':
        case 'i':
            return 1;
        case 'V':
        case 'v':
            return 5;
        case 'X':
        case 'x':
            return 10;
        case 'L':
        case 'l':
            return 50;
        case 'C':
        case 'c':
            return 100;
        case 'D':
        case 'd':
            return 500;
        case 'M':
        case 'm':
            return 1000;
        }
        return 0;
    }
};

class Solution {
public:
    int atoi(const char *str) {
        if (!str) return 0;
        long long value = 0;
        int i = 0;
        while (str[i] == ' ') i++;
        bool negative = str[i] == '-' ? true : false;
        if (str[i] == '+' || str[i] == '-') i++;
        while (str[i] != '\0') {
            if (str[i] >= '0' && str[i] <= '9') {
                value *= 10;
                value += (str[i] - '0');
                i++;
            } else
                break;
        }
        if (negative)
            value = -value;
        if (value > INT_MAX) return INT_MAX;
        if (value < INT_MIN) return INT_MIN;
        return (int)value;
    }
};

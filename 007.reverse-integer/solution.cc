class Solution {
public:
    int reverse(int x) {
        if (x == 0 || x == INT_MIN || x == INT_MAX) return x;
        int y = x > 0 ? x : -x;
        long long j = y % 10;
        y /= 10;
        while (y > 0) {
            j *= 10;
            j += (y % 10);
            y /= 10;
        }
        j = x > 0 ? j : -j;
        if (j > INT_MAX) return INT_MAX;
        if (j < INT_MIN) return INT_MIN;
        return (int)j;
    }
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        ListNode* i = head;
        ListNode* j = head;
        while (i && i->next) {
            i = i->next->next;
            j = j->next;
            if (i == j) break;
        }
        if (!i || !i->next) return NULL;
        j = head;
        while (i != j) {
            i = i->next;
            j = j->next;
        }
        return j;
    }
};

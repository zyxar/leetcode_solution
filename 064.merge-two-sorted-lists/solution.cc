/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
        if (!l1) return l2;
        if (!l2) return l1;
        ListNode head(0);
        ListNode* id, *it1 = l1, *it2 = l2;
        if (l1->val < l2->val) {
            head.next = l1;
            it1 = it1->next;
        } else {
            head.next = l2;
            it2 = it2->next;
        }
        id = head.next;
        while (it1 && it2) {
            if (it1->val < it2->val) {
                id->next = it1;
                it1 = it1->next;
            } else {
                id->next = it2;
                it2 = it2->next;
            }
            id = id->next;
        }
        if (it1) {
            id->next = it1;
        } else { // if (it2)
            id->next = it2;
        }
        return head.next;
    }
};

# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    # @param root, a tree node
    # @param sum, an integer
    # @return a list of lists of integers
    def pathSum(self, root, sum):
        if root is None:
            return []
        if root.left is None and root.right is None:
            if sum == root.val:
                return [[sum]]
            return []
        left = self.pathSum(root.left, sum - root.val)
        right = self.pathSum(root.right, sum - root.val)
        for i in left:
            i.insert(0, root.val)
        for i in right:
            i.insert(0, root.val)
        return left + right

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *removeNthFromEnd(ListNode *head, int n) {
        ListNode* i = head;
        ListNode* j = head;
        while (n > 1 && j != NULL) {
            j = j->next;
            n--;
        }
        if (j == NULL || j->next == NULL) {
            return head->next;
        }
        j = j->next;
        while (j->next != NULL) {
            i = i->next;
            j = j->next;
        }
        i->next = i->next->next;
        return head;
    }
};

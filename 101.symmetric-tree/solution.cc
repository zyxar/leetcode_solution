/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isSymmetric(TreeNode *root) {
        if (!root || (!root->left && !root->right)) return true;
        return isSymmetric(root->left, root->right);
    }
private:
    bool isSymmetric(TreeNode* left, TreeNode* right) {
        if ((!left && right) || (left && !right)) return false;
        if (!left && !right) return true;
        return (left->val == right->val) && isSymmetric(left->left, right->right) && isSymmetric(left->right, right->left);
    }
};
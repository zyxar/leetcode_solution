class Solution {
public:
    void sortColors(int A[], int n) {
        if (n < 2) return;
        int i = 0, j = 0, k = 0, h = 0;
        for (h = 0; h < n; h++) {
            if (A[h] == 0) i++;
            else if (A[h] == 1) j++;
            else k++;
        }
        h = 0;
        while (h < i) {
            A[h] = 0;
            h++;
        }
        h = 0;
        while (h < j) {
            A[i+h] = 1;
            h++;
        }
        h = 0;
        while (h < k) {
            A[i+j+h] = 2;
            h++;
        }
    }
};

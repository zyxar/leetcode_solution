/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *deleteDuplicates(ListNode *head) {
        head = next2(head);
        ListNode* prev = head;
        ListNode* current = NULL;
        while (1) {
            if (!prev) break;
            current = next2(prev->next);
            prev->next = current;
            prev = current;
        }
        return head;
    }
private:
    ListNode* next(ListNode *head) {
        if (!head) return NULL;
        if (head->next != NULL && head->val == head->next->val) {
            while (head != NULL && head->next != NULL && head->val == head->next->val) {
                head = head->next;
            }
            if (head)
                head = head->next;
        }
        return head;
    }
    ListNode* next2(ListNode* head) {
        ListNode* candidate = next(head);
        while (candidate != head && candidate) {
            head = candidate;
            candidate = next(head);
        }
        return candidate;
    }
};

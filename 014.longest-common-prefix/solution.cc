class Solution {
public:
    string longestCommonPrefix(vector<string> &strs) {
        size_t len = strs.size();
        if (len == 0) return "";
        if (len == 1) return strs[0];
        int i = 0, j = 0;
        char c;
        while (i < strs[0].length()) {
            c = strs[0][i];
            for (j = 1; j < len; j++) {
                if (i >= strs[j].length()) goto done;
                if (strs[j][i] != c) goto done;
            }
            i++;
        }
    done:
        return strs[0].substr(0, i);
    }
};

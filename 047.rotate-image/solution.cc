class Solution {
public:
    void rotate(vector<vector<int> > &matrix) {
        int n = matrix.size();
        if (n < 2) return;
        int tmp = 0;
        int j = 0;
        int k = 0;
        while (n-k > 1) {
            j = k;
            while (j < n-1) {
                tmp = matrix[j][n-1];
                matrix[j][n-1] = matrix[k][j];
                matrix[k][j] = matrix[n-j+k-1][k];
                matrix[n-j+k-1][k] = matrix[n-1][n-1-j+k];
                matrix[n-1][n-1-j+k] = tmp;
                j++;
            }
            k++;
            n--;
        }
    }
};

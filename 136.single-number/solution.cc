class Solution {
public:
    int singleNumber(int A[], int n) {
        int r = 0;
        int i = 0;
        while (i < n) {
            r ^= A[i++];
        }
        return r;
    }
};

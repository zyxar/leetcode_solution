class Solution {
public:
    bool isPalindrome(int x) {
        if (x == 0) return true;
        if (x < 0) return false;
        // if (x < 0) return isPalindrome(-x);
        int y = x;
        int j = y % 10;
        y /= 10;
        while (y > 0) {
            j *= 10;
            j += (y % 10);
            y /= 10;
        }
        if (j == x) return true;
        return false;
    }
};

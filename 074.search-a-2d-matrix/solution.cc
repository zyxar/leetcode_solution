class Solution {
public:
    bool searchMatrix(vector<vector<int> > &matrix, int target) {
        int i = find(matrix, target);
        if (i == -1) return false;
        return find(matrix[i], target, 0, matrix[i].size());
    }
private:
    bool find(const vector<int>& matrix, int target, int start, int end) {
        if (end <= start) return false;
        if (end - start == 1) {
            if (matrix[start] == target) return true;
            return false;
        }
        int index = (end-start)/2 + start;
        if (matrix[index] == target) return true;
        else if (matrix[index] < target) return find(matrix, target, index+1, end);
        return find(matrix, target, start, index);
    }
    int find(const vector<vector<int> > &matrix, int target) {
        size_t size = matrix.size();
        int i = 0;
        while (i < size && matrix[i][0] < target) i++;
        if (i < size && matrix[i][0] == target) return i;
        return i-1;
    }
};

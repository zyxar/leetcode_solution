/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *rotateRight(ListNode *head, int k) {
        if (!head || !head->next || k == 0) return head;
        ListNode* id = head;
        int n = 1;
        while (id->next) {
            ++n;
            id = id->next;
        }
        k %= n;
        if (k == 0) return head;
        id->next = head; // cycle;
        id = head;
        k = n-k;
        n = 1;
        while (n < k) {
            ++n;
            id = id->next;
        }
        ListNode* ret = id->next;
        id->next = NULL;
        return ret;
    }
};

class Solution {
public:
    void setZeroes(vector<vector<int> > &matrix) {
        size_t rows = matrix.size();
        if (rows == 0) return;
        size_t cols = matrix[0].size();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (matrix[i][j] < 0) matrix[i][j]--;
                else if (matrix[i][j] > 0) matrix[i][j]++;
            }
        } // backup
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (matrix[i][j] == 0) {
                    for (int k = 0; k < cols; k++) {
                        if (matrix[i][k] != 0) matrix[i][k] = -1;
                    }
                    for (int k = 0; k < rows; k++) {
                        if (matrix[k][j] != 0) matrix[k][j] = -1;
                    }
                }
            }
        } // spread
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (matrix[i][j] == INT_MIN) matrix[i][j]--;
                else if (matrix[i][j] < 0) matrix[i][j]++;
                else if (matrix[i][j] == INT_MAX) matrix[i][j]++;
                else if (matrix[i][j] > 0) matrix[i][j]--;
            }
        } // restore        
    }
};

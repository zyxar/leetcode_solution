/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *deleteDuplicates(ListNode *head) {
        ListNode* walk = head;
        while (walk != NULL && walk->next != NULL) {
            while (walk->next != NULL && walk->val == walk->next->val) {
                walk->next = walk->next->next;
            }
            while (walk != NULL && walk->next != NULL && walk->val != walk->next->val) {
                walk = walk->next;
            }
        }
        return head;
    }
};

#!/bin/bash

TXT_FILE=".index.txt"
HTML_FILE=".index.html"
OJ_URL="https://oj.leetcode.com/problems/"

rm -f ${HTML_FILE} ${TXT_FILE}
wget ${OJ_URL} -nv -O ${HTML_FILE} -o /dev/null
[[ $? != 0 ]] && (echo -e "\e[31mError in fetching problems.\e[0m"; exit 1)

cat ${HTML_FILE} | grep "problems" | grep td | cut -d '"' -f 2 | cut -d '/' -f 3 > ${TXT_FILE}
v=$(cat ${TXT_FILE} | wc -l)
echo -e "\e[36m${v} problems online.\e[0m"

cat ${TXT_FILE} | while read line;do
  dir="$(printf '%03d' $v).$line"
  [[ ! -d ${dir} ]] && (echo mkdir ${dir}; mkdir ${dir})
  ((v--))
done

echo -e "\e[32mDONE.\e[0m"
rm -f ${HTML_FILE} ${TXT_FILE}

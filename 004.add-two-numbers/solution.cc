/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        ListNode* head = NULL;
        ListNode* tail = NULL;
        bool carrier = false;
        int sum = l1->val + l2->val;
        head = new ListNode(sum%10);
        tail = head;
        if (sum > 9) carrier = true;
        l1 = l1->next;
        l2 = l2->next;
        while (l1 != NULL && l2 != NULL) {
            sum = l1->val + l2->val;
            if (carrier) sum += 1;
            carrier = false;
            tail->next = new ListNode(sum%10);
            tail = tail->next;
            if (sum > 9) carrier = true;
            l1 = l1->next;
            l2 = l2->next;
        }
        while (l1 != NULL) {
            sum = l1->val;
            if (carrier) sum += 1;
            carrier = false;
            tail->next = new ListNode(sum%10);
            tail = tail->next;
            if (sum > 9) carrier = true;
            l1 = l1->next;
        }
        while (l2 != NULL) {
            sum = l2->val;
            if (carrier) sum += 1;
            carrier = false;
            tail->next = new ListNode(sum%10);
            tail = tail->next;
            if (sum > 9) carrier = true;
            l2 = l2->next;
        }
        if (carrier) {
            tail->next = new ListNode(1);
            tail = tail->next;
        }
        return head;
    }
};

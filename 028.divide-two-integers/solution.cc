class Solution {
public:
    int divide(int dividend, int divisor) {
        if (dividend == 0) return 0;
        long long div = abs((long long)dividend);
        long long dor = abs((long long)divisor);
        bool negative = false;
        if (dividend < 0) negative = !negative;
        if (divisor < 0) negative = !negative;
        if (div == dor) return negative ? -1 : 1;
        if (div < dor) return 0;
        int ret = 0, j = 0;
        while (dor <= div) {
            j++;
            dor <<= 1;
        }
        j--;
        dor >>= 1;
        while (j >= 0) {
            if (div >= dor) {
                ret += (1 << j);
                div -= dor;
            }
            dor >>= 1;
            j--;
        }
        return negative ? -ret : ret;
    }
};

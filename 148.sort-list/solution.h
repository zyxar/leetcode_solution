/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *sortList(ListNode *head) {
        if (!head || !head->next) return head;
        ListNode* i = head;
        ListNode* j = head;
        while (j && j->next && j->next->next) {
            i = i->next;
            j = j->next->next;
        }
        j = i->next;
        i->next = NULL;
        i = head;
        ListNode* left = sortList(i);
        ListNode* right = sortList(j);
        return merge(left, right);
    }
private:
    ListNode* merge(ListNode* h1, ListNode* h2) {
        if (!h2) return h1;
        if (!h1) return h2;
        ListNode* head;
        if (h1->val <= h2->val) {
            head = h1;
            h1 = h1->next;
        } else {
            head = h2;
            h2 = h2->next;
        }
        ListNode* it = head;
        while (h1 && h2) {
            if (h1->val <= h2->val) {
                it->next = h1;
                h1 = h1->next;
            } else {
                it->next = h2;
                h2 = h2->next;
            }
            it = it->next;
        }
        if (h1) {
            it->next = h1;
        }
        if (h2) {
            it->next = h2;
        }
        return head;
    }
};

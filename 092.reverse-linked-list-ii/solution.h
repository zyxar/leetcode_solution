/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *reverseBetween(ListNode *head, int m, int n) {
        if (!head || !head->next || m == n) return head;
        ListNode node(-1);
        node.next = head;
        ListNode* id = &node;
        for (int i = 1; i < m; ++i) {
            id = id->next;
        } // id, id->next
        head = id->next;
        ListNode* next = head->next;
        ListNode* s;
        for (int i = m; i < n; ++i) {
            s = next->next;
            next->next = head;
            head = next;
            next = s;
        }
        id->next->next = next;
        id->next = head;
        return node.next;
    }
};

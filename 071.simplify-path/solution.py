class Solution:
    # @param path, a string
    # @return a string
    def simplifyPath(self, path):
      if len(path) == 0:
        return '';
      r = []
      a = filter(lambda x: x!='.' and x !='', path.split('/'))
      for i in a:
        if i != '..':
          r.append(i)
        else:
          if len(r) > 0:
            r.pop()
      return '/'+'/'.join(r)

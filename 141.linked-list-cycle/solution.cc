/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool hasCycle(ListNode *head) {
        ListNode* i = head;
        ListNode* j = head;
        while (i && i->next) {
            i = i->next->next;
            j = j->next;
            if (i == j)
                break;
        }
        return !(i == NULL || i->next == NULL);
    }
};

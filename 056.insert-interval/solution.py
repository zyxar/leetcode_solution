# Definition for an interval.
# class Interval:
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

class Solution:
    # @param intervals, a list of Intervals
    # @param newInterval, a Interval
    # @return a list of Interval
    def insert(self, intervals, newInterval):
        intervals.append(newInterval);
        if len(intervals) == 1:
            return intervals
        intervals = sorted(intervals, self.compare)
        r = [intervals[0]]
        for it in intervals[1:]:
            if (r[-1].end >= it.start):
                r[-1].start = min(r[-1].start, it.start);
                r[-1].end = max(r[-1].end, it.end)
            else:
                r.append(it)
        return r
    def compare(self, a, b):
        return cmp(a.start, b.start)

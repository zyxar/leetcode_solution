class Solution {
public:
    int removeDuplicates(int A[], int n) {
        if (n < 2) return n;
        int j = 0, s = 0, k = 0;
        while (s < n) {
            k = s+1;
            while (k < n && A[k] == A[s]) ++k;
            if (k >= n) break;
            ++j;
            A[j] = A[k];
            s = k;
        }
        return j+1;
    }
};

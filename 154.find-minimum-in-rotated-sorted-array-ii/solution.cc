class Solution {
public:
    int findMin(vector<int> &num) {
        int j = num.size() - 1;
        if (j == 0) return num[0];
        while (j > 0 && num[j] >= num[j-1]) {
            j--;
        }
        return num[j];
    }
};

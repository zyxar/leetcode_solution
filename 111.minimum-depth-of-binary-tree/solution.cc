/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int minDepth(TreeNode *root) {
        if (!root) return 0;
        if (!root->left && !root->right) return 1;
        int left = 0, right = 0;
        if (root->left) {
            left = minDepth(root->left);
        }
        if (root->right) {
            right = minDepth(root->right);
        }
        int ret = left;
        if (left == 0)
            ret = right;
        else {
            if (right > 0 && right < left) {
                ret = right;
            }
        }
        return ret + 1;
    }
};

class Solution {
public:
    int minPathSum(vector<vector<int> > &grid) {
        size_t n = grid.size();
        if (n == 0) return 0;
        size_t m = grid[0].size();
        if (m == 0) return 0;
        int i = 1, j = 1;
        while (j < m) {
            grid[0][j] += grid[0][j-1];
            j++;
        }
        while (i < n) {
            grid[i][0] += grid[i-1][0];
            i++;
        }
        for (i = 1; i < n; ++i) {
            for (j = 1; j < m; ++j) {
                int min = grid[i-1][j] < grid[i][j-1] ? grid[i-1][j] : grid[i][j-1];
                grid[i][j] += min;
            }
        }
        return grid[n-1][m-1];
    }
};

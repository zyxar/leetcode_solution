class Solution {
public:
    int minimumTotal(vector<vector<int> > &triangle) {
        size_t rows = triangle.size();
        if (rows == 0) return 0;
        int i = 1;
        int len = 0;
        while (i < rows) {
            len = triangle[i].size();
            triangle[i][0] += triangle[i-1][0];
            triangle[i][len-1] += triangle[i-1][len-2];
            for (int j = 1; j < len-1; ++j) {
                int min = triangle[i-1][j-1] < triangle[i-1][j] ? triangle[i-1][j-1] : triangle[i-1][j];
                triangle[i][j] += min;
            }
            ++i;
        }
        int min = triangle[rows-1][0];
        for (int j = 0; j < len; ++j) {
            if (min > triangle[rows-1][j]) min = triangle[rows-1][j];
        }
        return min;
    }
};

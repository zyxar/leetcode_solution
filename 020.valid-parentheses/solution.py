class Solution:
    # @return a boolean
    def isValid(self, s):
        cache = []
        for i in s:
            if i == '(' or i == '{' or i == '[':
                cache.append(i);
                continue
            if len(cache) == 0:
                return False
            v = cache.pop();
            if (v == '(' and i != ')') or (v == '[' and i != ']') or (v == '{' and i != '}'):
                return False;
        return len(cache) == 0

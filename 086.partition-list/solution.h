/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *partition(ListNode *head, int x) {
        if (!head || !head->next) return head;
        ListNode *lnode = NULL, *rnode = NULL;
        if (head->val < x) {
            lnode = head;
            rnode = right(head->next, x);
            if (!rnode) return head;
            head = head->next;
        } else {
            rnode = head;
            lnode = left(head->next, x);
            if (!lnode) return head;
            head = head->next;
        }
        ListNode* lit = lnode;
        ListNode* rit = rnode;
        while (head) {
            if (head != lnode && head != rnode) {
                if (head->val < x) {
                    lit->next = head;
                    lit = head;
                } else {
                    rit->next = head;
                    rit = head;
                }
            }
            head = head->next;
        }
        lit->next = rnode;
        rit->next = NULL;
        return lnode;
    }
private:
    ListNode* left(ListNode* head, int x) {
        while (head) {
            if (x > head->val) {
                return head;
            }
            head = head->next;
        }
        return NULL;
    }

    ListNode* right(ListNode* head, int x) {
        while (head) {
            if (x <= head->val) {
                return head;
            }
            head = head->next;
        }
        return NULL;
    }
};

class Solution {
public:
    int removeElement(int A[], int n, int elem) {
        int k = 0, i = 0;
        for (i = 0; i < n; i++) {
            if (A[i] == elem) k++;
        }
        int ret = n - k;
        int needle = n - 1;
        while (A[needle] == elem) {
            needle--;
            k--;
        }
        int index = 0;
        while (k > 0) {
            while (A[index] != elem) {
                index++;
            }
            if (index >= needle) break;
            A[index] = A[needle];
            needle--;
            k--;
            index++;
            while (A[needle] == elem) {
                needle--;
                k--;
            }
        }
        return ret;
    }
};

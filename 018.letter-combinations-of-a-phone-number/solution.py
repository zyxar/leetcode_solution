class Solution:
    # @return a list of strings, [s1, s2]
    def letterCombinations(self, digits):
      a = [d[i] for i in digits]
      v = ['']
      for j in a:
        v = self.combine(v, j)
      return v
    
    def combine(self, l1, l2):
      r = []
      for i in l1:
        for j in l2:
          r.append(i+j)
      return r

d = {
  '0': [' '],
  '2': [i for i in 'abc'],
  '3': [i for i in 'def'],
  '4': [i for i in 'ghi'],
  '5': [i for i in 'jkl'],
  '6': [i for i in 'mno'],
  '7': [i for i in 'pqrs'],
  '8': [i for i in 'tuv'],
  '9': [i for i in 'wxyz']
}

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *reverseKGroup(ListNode *head, int k) {
        if (!head || !head->next || k == 1) return head;
        ListNode* id = head;
        int n = 1;
        while (id->next) {
            ++n;
            id = id->next;
        }
        if (k > n) return head;
        ListNode node(0);
        ListNode* it = &node;
        while (k <= n) {
            it->next = reverse(head, k, &id);
            it = head;
            head = id;
            n -= k;
        }
        if (n > 0) {
            it->next = head;
        }
        return node.next;
    }
private:
    ListNode* reverse(ListNode* head, int m, ListNode** next) {
        int c = 1;
        ListNode* i = head;
        ListNode* j = head->next;
        head->next = NULL;
        ListNode* s = NULL;
        while (c < m) {
            ++c;
            s = j->next;
            j->next = i;
            i = j;
            j = s;
        }
        *next = s;
        return i;
    }
};

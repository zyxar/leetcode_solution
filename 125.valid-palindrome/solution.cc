class Solution {
public:
    bool isPalindrome(string s) {
        int len = s.length();
        if (len < 2) return true;
        int i = 0, j = len-1;
        while (i < j) {
            while (i < j && !valid(s[i])) i++;
            while (i < j && !valid(s[j])) j--;
            if (i == j) return true;
            if (!equal(s[i], s[j])) return false;
            i++;
            j--;
        }
        return true;
    }
private:
    bool equal(char c, char d) {
        if (((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) && ((d >= 'A' && d <= 'Z') || (d >= 'a' && d <= 'z'))) {
            if (c == d || (c^' ') == d) return true;
        } else if ((c >= '0' && c <= '9') && (d >= '0' && d <= '9')) {
            if (c == d) return true;
        }
        return false;
    }
    bool valid(char c) {
        if (((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) || (c >= '0' && c <= '9')) {
          return true;
        }
        return false;
    }
};

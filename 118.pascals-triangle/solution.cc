class Solution {
public:
    vector<vector<int> > generate(int numRows) {
        vector<vector<int> > ret;
        if (numRows <= 0)
            return ret;
        vector<int> j;
        j.push_back(1);
        ret.push_back(j);
        int i = 0;
        for (i = 0; i < numRows - 1; ++i) {
            j = next(j);
            ret.push_back(j);
        }
        return ret;
    }
private:
    vector<int> next(const vector<int>& prev) {
        vector<int> ret;
        ret.push_back(1);
        int i;
        int size = prev.size();
        for (i = 0; i < size - 1; ++i) {
            ret.push_back(prev[i]+prev[i+1]);
        }
        ret.push_back(1);
        return ret;
    }
};

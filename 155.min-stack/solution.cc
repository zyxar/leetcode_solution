class MinStack {
public:
    void push(int x) {
        vals.push(x);
        if (mins.empty() || x <= mins.top()) {
            mins.push(x);
        }
    }

    void pop() {
        if (vals.empty()) return;
        int x = vals.top();
        if (!mins.empty() && x == mins.top()) {
            mins.pop();
        }
        vals.pop();
    }

    int top() {
        if (vals.empty()) return 0;
        return vals.top();
    }

    int getMin() {
        if (mins.empty()) return 0;
        return mins.top();
    }
private:
    stack<int> vals;
    stack<int> mins;
};

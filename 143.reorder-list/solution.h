/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    void reorderList(ListNode *head) {
        if (!head || !head->next || !head->next->next)
            return;
        ListNode* it = head;
        ListNode* half = head;
        ListNode* end = head;
        while (it && it->next) {
            end = half;
            half = half->next;
            it = it->next->next;
        }
        end->next = NULL;
        half = reverse(half);
        ListNode* tmp1 = NULL;
        ListNode* tmp2 = NULL;
        it = head;
        while (it) {
            tmp1 = it->next;
            it->next = half;
            tmp2 = half->next;
            if (tmp1)
                half->next = tmp1;
            it = tmp1;
            half = tmp2;
        }
    }
private:
    ListNode* reverse(ListNode* head) {
        if (!head || !head->next) return head;
        ListNode* i = head;
        ListNode* j = head->next;
        head->next = NULL;
        ListNode* s = NULL;
        while (i && j) {
            s = j->next;
            j->next = i;
            i = j;
            j = s;
        }
        return i;
    }
};

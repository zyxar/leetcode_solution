class Solution {
public:
    int findMin(vector<int> &num) {
        int j = num.size() - 1;
        int i = j - 1;
        while (i >= 0 && num[j] > num[i]) {
            j--;
            if (i > 0 ) i--;
        }
        return num[j];
    }
};

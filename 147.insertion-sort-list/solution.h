/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *insertionSortList(ListNode *head) {
        if (!head || !head->next) return head;
        ListNode node(-1);
        node.next = head;
        head = head->next;
        node.next->next = NULL;
        ListNode* s = NULL;
        ListNode* p = NULL;
        ListNode* id = NULL;
        while (head) {
            id = &node;
            while (id->next && id->next->val <= head->val) id = id->next;
            s = head->next;
            p = id->next;
            id->next = head;
            head->next = p;
            head = s;
        }
        return node.next;
    }
};

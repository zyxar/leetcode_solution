class Solution {
public:
    vector<int> spiralOrder(vector<vector<int> > &matrix) {
        vector<int> vec;
        size_t n = matrix.size();
        if (n == 0) return vec;
        if (n == 1) return matrix[0];
        size_t m = matrix[0].size();
        int i = 0, j = 0; // i ~ m, j ~ n
        while (m-1 > i && n-1 > j) {
            int k = i;
            while(k < m-1) {
                vec.push_back(matrix[j][k]);
                k++;
            }
            k = j;
            while (k < n-1) {
                vec.push_back(matrix[k][m-1]);
                k++;
            }
            k = m-1;
            while (k > i) {
                vec.push_back(matrix[n-1][k]);
                k--;
            }
            k = n-1;
            while (k > j) {
                vec.push_back(matrix[k][i]);
                k--;
            }
            i++; j++;
            m--; n--;
        }
        if (m > i && n > j) {
          if (m-1 > i) {
            while (m > i) {
              vec.push_back(matrix[j][i]);
              i++;
            }
          } else if (n-1 > j) {
            while (n > j) {
              vec.push_back(matrix[j][i]);
              j++;
            }
          } else {
            vec.push_back(matrix[j][i]);
          }
        }
        return vec;
    }
};

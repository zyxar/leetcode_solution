class Solution {
public:
    int maxProfit(vector<int> &prices) {
        size_t len = prices.size();
        if (len < 2) return 0;
        int start = 0;
        int buy = INT_MAX;
        int pv = 0;
        while (start < len) {
            int t = nextTrough(prices, start);
            int p = nextPeak(prices, t);
            if (p >= len) return pv;
            if (buy > prices[t]) buy = prices[t];
            if (pv < prices[p] - buy) pv = prices[p] - buy;
            start = p+1;
        }
        return pv;
    }
private:
    int nextTrough(vector<int> &v, int start = 0) {
        size_t len = v.size();
        while (start+1 < len && v[start] >= v[start+1]) ++start;
        return start;
    }
    int nextPeak(vector<int> &v, int start = 0) {
        size_t len = v.size();
        while (start+1 < len && v[start] <= v[start+1]) ++start;
        return start;
    }
};

class Solution {
public:
    vector<int> plusOne(vector<int> &digits) {
        size_t len = digits.size();
        if (len == 0) {
            digits.push_back(1);
            return digits;
        }
        int carry = 1;
        int j = len-1;
        int v;
        while (j >= 0) {
            v = digits[j]+carry;
            if (v > 9) {
                v %= 10;
                carry = 1;
            } else {
                carry = 0;
            }
            digits[j] = v;
            j--;
        }
        if (carry) {
            digits.insert(digits.begin(), 1);
        }
        return digits;
    }
};
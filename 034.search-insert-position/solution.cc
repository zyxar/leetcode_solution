class Solution {
public:
    int searchInsert(int A[], int n, int target) {
        if (n == 0) return 0;
        int i = 0;
        while (i < n && A[i] < target) i++;
        if (A[i] == target) return i;
        if (A[i] < target) return n;
        // if (A[i] > target) 
        return i;
    }
};

class Solution {
public:
    int climbStairs(int n) {
        if (n <= 1) return 1;
        if (n == 2) return 2;
        int p = 0;
        int a = 1;
        int b = 1;
        for (int i = 2; i <= n; ++i) { 
            p = a + b;
            a = b;
            b = p;
        }
        return p;
    }
};

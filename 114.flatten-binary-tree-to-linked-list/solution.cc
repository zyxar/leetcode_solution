/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void flatten(TreeNode *root) {
        if (!root) return;
        if (!root->right) {
            root->right = root->left;
            root->left = NULL;
            return flatten(root->right);
        }
        if (!root->left)
            return flatten(root->right);
        TreeNode* last = root->left;
        while (last->right)
            last = last->right;
        last->right = root->right;
        root->right = root->left;
        root->left = NULL;
        return flatten(root->right);
    }
};

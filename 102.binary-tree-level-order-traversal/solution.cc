/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int> > levelOrder(TreeNode *root) {
        vector<vector<int> > vec;
        levelOrder(root, vec, 0);
        return vec;
    }
private:
    void levelOrder(TreeNode *root, vector<vector<int> >& vec, int depth) {
        if (!root) return;
        size_t size = vec.size();
        if (depth == size) {
            vector<int> v;
            v.push_back(root->val);
            vec.push_back(v);
        } else {
            vec[depth].push_back(root->val);
        }
        if (root->left) {
            levelOrder(root->left, vec, depth+1);
        }
        if (root->right) {
            levelOrder(root->right, vec, depth+1);
        }
    }
};
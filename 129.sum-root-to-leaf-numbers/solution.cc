/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int sumNumbers(TreeNode *root) {
        if (!root) return 0;
        walk(root, 0);
        int sum = 0;
        for (vector<int>::iterator it = m.begin(); it != m.end(); it++) {
            sum += (*it);
        }
        return sum;
    }
private:
    vector<int> m;
    void walk(TreeNode *root, int carry) {
        carry += root->val;
        if (!root->left && !root->right) {
            m.push_back(carry);
        }
        if (root->left)
            walk(root->left, carry*10);
        if (root->right)
            walk(root->right, carry*10);
    }
};

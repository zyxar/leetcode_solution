class Solution {
public:
    int lengthOfLastWord(const char *s) {
        if (!s) return 0;
        int i = 0;
        while (s[i] != '\0') i++;
        while (i > 0 && s[i-1] == ' ') i--;
        if (i == 0) return 0;
        int j = i-1;
        while (j > 0 && s[j] != ' ') j--;
        if (s[j] == ' ') j++;
        return i-j;
    }
};

class Solution:
    # @return a list of integers
    def getRow(self, rowIndex):
        return [self.c(rowIndex,x) for x in range(0, rowIndex+1)]

    def c(self, n, k):
      if k == 0 or k == n:
        return 1
      if 2*k <= n:
        i = 0
        s1 = 1
        s2 = 1
        while i < k:
          s1 *= (n-i)
          s2 *= (k-i)
          i += 1
        return s1/s2
      else:
        return self.c(n, n-k)

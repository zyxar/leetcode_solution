class Solution {
public:
    double pow(double x, int n) {
        if (n == 0 || x == 1) return (double)1.0;
        double v = pow(x, n/2);
        if (n % 2 == 0) {
            return v*v;
        } else if (n > 0) {
            return x*v*v;
        } else {
            return v*v/x;
        }
    }
};
class Solution {
public:
    string addBinary(string a, string b) {
        string s;
        size_t la = a.length();
        size_t lb = b.length();
        size_t len = (la > lb ? la : lb) + 1;
        s.resize(len);
        int i = la-1, j = lb-1, k = len-1;
        short carry = 0;
        while (i >= 0 && j >= 0) {
            short v = (a[i] - '0') + (b[j] - '0') + carry;
            if (v > 1) {
                carry = 1;
                v %= 2;
            } else {
                carry = 0;
            }
            s[k] = v + '0';
            k--;
            i--;
            j--;
        }
        while (i >= 0) {
            short v = (a[i] - '0') + carry;
            if (v > 1) {
                carry = 1;
                v %= 2;
            } else {
                carry = 0;
            }
            s[k] = v + '0';
            k--;
            i--;
        }
        while (j >= 0) {
            short v = (b[j] - '0') + carry;
            if (v > 1) {
                carry = 1;
                v %= 2;
            } else {
                carry = 0;
            }
            s[k] = v + '0';
            k--;
            j--;
        }
        if (carry) {
            s[0] = '1';
        } else {
            s.erase(0, 1);
        }
        return s;
    }
};
class Solution {
public:
    int search(int A[], int n, int target) {
        if (n == 0) return -1;
        int i = 0;
        if (target < A[0] && target > A[n-1]) return -1;
        if (target >= A[0]) {
            while (A[i] < target && i < n) i++;
            if (A[i] == target) return i;
            return -1;
        } else if (target <= A[n-1]) {
            i = n-1;
            while (i > 0 && A[i] > target) i--;
            if (A[i] == target) return i;
            return -1;
        }
        return -1;
    }
};

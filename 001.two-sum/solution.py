class Solution:
    # @return a tuple, (index1, index2)
    def twoSum(self, num, target):
        i = 0
        d = {}
        while i < len(num):
            if target - num[i] in d:
                return (d[target - num[i]], i+1)
            else:
                d[num[i]] = i+1
            i += 1

class Solution {
public:
    int strStr(char *haystack, char *needle) {
        char c, sc;
      size_t len;
        char* s = haystack;
      if ((c = *needle++) != '\0') {
        len = strlen(needle);
        do {
          do {
            if ((sc = *s++) == '\0')
              return -1;
          } while (sc != c);
        } while (strncmp(s, needle, len) != 0);
        s--;
      }
      return (int)(s - haystack);
    }
};

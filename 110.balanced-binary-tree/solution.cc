/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isBalanced(TreeNode *root) {
        int depth = 0;
        return isBalanced(root, depth);
    }
private:
    bool isBalanced(TreeNode* root, int& depth) {
        if (!root) {
            depth = 0;
            return true;
        }
        int left, right;
        if (isBalanced(root->left, left) && isBalanced(root->right, right)) {
            int diff = right > left ? right - left : left - right;
            if (diff < 2) {
                depth = (right > left ? right : left) + 1;
                return true;
            }
            return false;
        }
    }
};